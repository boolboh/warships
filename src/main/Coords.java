package main;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class Coords {
    int x, y;


    public Coords(String s) throws IllegalArgumentException{
        String[] coords = s.split("\\D+");
        if(coords.length != 2) throw new IllegalArgumentException();
        this.x = Integer.parseInt(coords[0]);
        this.y = Integer.parseInt(coords[1]);
    }

    public Coords(int x, int y) {
        this.x = x;
        this.y = y;
    }


    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }
}
