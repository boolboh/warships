package main;

public class GameData {
    private Player[] players = new Player[]{new Player("Bob"), new Player("Jorge")};
    private Player activePlayer = this.players[0];
    private GameStage gs = new InitGameStage();
    private GameStage initGs = gs;
    private Window window = new Window();

    public Player[] getPlayers() {
        return players;
    }

    public Player getActivePlayer() {
        return activePlayer;
    }

    public void changeActivePlayer() {
        this.activePlayer = getOtherPlayer(activePlayer);
    }

    public Player getOtherPlayer(Player player) {
        return player == players[0] ? players[1] : players[0];
    }

    public Boolean isFirstPlayer(Player player) {
        return player == players[0];
    }

    public Boolean isFinished() {
        return (players[0].ifLost() || players[1].ifLost()) && (gs != initGs);
    }

    public void runStage() {
        gs.run(this);
    }

    public void setGs(GameStage gs) {
        this.gs = gs;
    }

    public Window getWindow() {
        return window;
    }
}
