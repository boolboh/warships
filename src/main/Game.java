package main;

public class Game {
    private GameData gd = new GameData();

    public void run() {
        // InitGameStage
        gd.runStage();

        //StartGameStage and MidlleGameStage
        do {
            gd.runStage();
        } while(!gd.isFinished());

        // EndGameStage
        gd.runStage();
    }
}
