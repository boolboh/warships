package main;

public abstract class GameStage {
    public abstract void run(GameData gd);
    public abstract void nextStage(GameData gd);
}
