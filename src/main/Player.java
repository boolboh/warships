package main;

public class Player {
    private String name;
    private Board board;

    public Player(String name) {
        this.name = name;
    }

    public Board getBoard() {
        return board;
    }

    public void setBoard(Board board) {
        this.board = board;
    }

    public Boolean ifLost() { return board.allShipsSunk(); }
}
