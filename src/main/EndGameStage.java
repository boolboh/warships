package main;

public class EndGameStage extends GameStage{
    @Override
    public void run(GameData gd) {
        Window window = gd.getWindow();
        window.buildData(gd);
        window.print();

        window.printTextActivePlayer(gd, "WYGRAŁEŚ! Gratulacje!", "Przegrałeś :C Get good");
    }

    @Override
    public void nextStage(GameData gd) {}
}
