package main;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class StartGameStage extends GameStage{
    @Override
    public void run(GameData gd) {
        // Player 1
        initShips(gd);
        gd.changeActivePlayer();

        // Player 2
        initShips(gd);
        gd.changeActivePlayer();

        nextStage(gd);
    }

    private void initShips(GameData gd) {
        Player player = gd.getActivePlayer();
        int n = player.getBoard().n();
        Window window = gd.getWindow();

        // Calculating how many ships each players have
        Map<Integer, Integer> shipNumbersByTypes = shipTypes(n);
        window.buildData(gd);
        window.print();


        Integer[] lengths = shipNumbersByTypes.keySet().toArray(new Integer[0]);
        Arrays.sort(lengths, Collections.reverseOrder());
        for (int len : lengths) {
            for (int j = 0; j < shipNumbersByTypes.get(len); j++) {
                initShip(gd, len);
            }
        }
    }

    private void initShip(GameData gd, int len) {
        Player player = gd.getActivePlayer();
        int n = player.getBoard().n();
        Window window = gd.getWindow();

        Coords[] shipCoords = new Coords[]{new Coords(0, 0), new Coords(n, n)}; // not proper coords
        do {
            try {
                shipCoords = len == 1 ? getRaftCoords(gd) : getShipCoords(gd, len);
                if (!player.getBoard().validateShip(shipCoords[0], shipCoords[1], len)) {
                    window.printTextActivePlayer(gd, "Błąd w określaniu współrzędnych.");
                }
            } catch (IllegalArgumentException iae) {
                window.printTextActivePlayer(gd, "Błąd w określaniu współrzędnych.");
            }
        } while (!player.getBoard().validateShip(shipCoords[0], shipCoords[1], len));

        player.getBoard().addShip(shipCoords[0], shipCoords[1], len);
        window.buildData(gd);
        window.print();
    }

    private Coords[] getRaftCoords(GameData gd) throws IllegalArgumentException {
        String begText = "Podaj dwie współrzędne statku o długości 1 i naciśnij enter:";

        Window window = gd.getWindow();
        Player player = gd.getActivePlayer();

        window.printTextActivePlayer(gd, begText);
        Coords raft = new Coords(window.readLine(gd));

        return new Coords[]{raft, raft};
    }

    private Coords[] getShipCoords(GameData gd, int len) throws IllegalArgumentException {
        String begText = "Podaj dwie współrzędne na początek statku o długości " + len + " i naciśnij enter:";
        String endText = "Podaj dwie współrzędne na koniec statku o długości " + len + " i naciśnij enter:";

        Player player = gd.getActivePlayer();
        Window window = gd.getWindow();

        window.printTextActivePlayer(gd, begText);
        Coords beg = new Coords(window.readLine(gd));
        window.printTextActivePlayer(gd, endText);
        Coords end = new Coords(window.readLine(gd));

        return new Coords[]{beg, end};
    }


    private Map<Integer, Integer> shipTypes(int n) {
        Map<Integer, Integer> map = new HashMap<>();
        int power = n / 2 + 1;
        int ships_number = 1;
        for (int i = n / 2 + 1; i >= 1; i--) {
            map.put(i, ships_number);
            if (i <= power / 2 + 1) {
                power = power / 2 + 1;
                ships_number++;
            }
        }
        return map;
    }

    @Override
    public void nextStage(GameData gd) {
        gd.setGs(new MiddleGameStage());
    }
}
