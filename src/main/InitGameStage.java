package main;

public class InitGameStage extends GameStage {
    public final int MAX_N = 40;
    public final int MIN_N = 2;

    @Override
    public void run(GameData gd) {
        int n;
        Window window = gd.getWindow();
        do {
            n = window.initN();
        } while (!validateN(n));

        for (Player player : gd.getPlayers()) {
            player.setBoard(new Board(n));
        }

        nextStage(gd);
    }



    private Boolean validateN(int n) {
        return n <= MAX_N && n >= MIN_N;
    }

    @Override
    public void nextStage(GameData gd) {
        gd.setGs(new StartGameStage());
    }
}
