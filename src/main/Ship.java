package main;

public class Ship {
    private int length;
    private int hitsCount;

    public Ship(int length) {
        this.length = length;
        this.hitsCount = 0;
    }

    public Boolean isSunk() {
        return length == hitsCount;
    }

    public void hit() {
        hitsCount++;
    }
}
